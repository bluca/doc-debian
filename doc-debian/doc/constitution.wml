
<h1>Constitution for the Debian Project (v1.9)</h1>

<p>Version 1.9 ratified on March 26th, 2022.</p>

<p>Supersedes
<a href="constitution.1.8">Version 1.8</a> ratified on January 28th, 2022,
<a href="constitution.1.7">Version 1.7</a> ratified on August 14th, 2016,
<a href="constitution.1.6">Version 1.6</a> ratified on December 13th, 2015,
<a href="constitution.1.5">Version 1.5</a> ratified on January 9th, 2015,
<a href="constitution.1.4">Version 1.4</a> ratified on October 7th, 2007,
<a href="constitution.1.3">Version 1.3</a> ratified on September 24th, 2006,
<a href="constitution.1.2">Version 1.2</a> ratified on October 29th, 2003,
<a href="constitution.1.1">Version 1.1</a> ratified on June 21st, 2003,
and <a href="constitution.1.0">Version 1.0</a> ratified on December 2nd,
1998.</p>

<toc-display />

<h2><toc-add-entry name="item-1">1. Introduction</toc-add-entry></h2>

<p><cite>The Debian Project is an association of individuals who have
made common cause to create a free operating system.</cite></p>

<p>This document describes the organisational structure for formal
decision-making in the Project.  It does not describe the goals of the
Project or how it achieves them, or contain any policies except those
directly related to the decision-making process.</p>

<h2><toc-add-entry name="item-2">2. Decision-making bodies and individuals</toc-add-entry></h2>

<p>Each decision in the Project is made by one or more of the
following:</p>

<ol>
  <li>The Developers, by way of General Resolution or an election;</li>

  <li>The Project Leader;</li>

  <li>The Technical Committee and/or its Chair;</li>

  <li>The individual Developer working on a particular task;</li>

  <li>Delegates appointed by the Project Leader for specific
  tasks;</li>

  <li>The Project Secretary.</li>
</ol>

<p>Most of the remainder of this document will outline the powers of
these bodies, their composition and appointment, and the procedure for
their decision-making.  The powers of a person or body may be subject to
review and/or limitation by others; in this case the reviewing body or
person's entry will state this.  <cite>In the list above, a person or
body is usually listed before any people or bodies whose decisions they
can overrule or who they (help) appoint - but not everyone listed
earlier can overrule everyone listed later.</cite></p>

<h3>2.1. General rules</h3>

<ol>
  <li>
    <p>Nothing in this constitution imposes an obligation on anyone to
    do work for the Project.  A person who does not want to do a task
    which has been delegated or assigned to them does not need to do
    it.  However, they must not actively work against these rules and
    decisions properly made under them.</p>
  </li>

  <li>
    <p>A person may hold several posts, except that the Project Leader,
    Project Secretary and the Chair of the Technical Committee must
    be distinct, and that the Leader cannot appoint themselves as their
    own Delegate.</p>
  </li>

  <li>
    <p>A person may leave the Project or resign from a particular post
    they hold, at any time, by stating so publicly.</p>
  </li>
</ol>

<h2><toc-add-entry name="item-3">3. Individual Developers</toc-add-entry></h2>

<h3>3.1. Powers</h3>

<p>An individual Developer may</p>

<ol>
  <li>make any technical or nontechnical decision with regard to their
  own work;</li>

  <li>propose or sponsor draft General Resolutions;</li>

  <li>propose themselves as a Project Leader candidate in
  elections;</li>

  <li>vote on General Resolutions and in Leadership elections.</li>
</ol>

<h3>3.2. Composition and appointment</h3>

<ol>
  <li>
    <p>Developers are volunteers who agree to further the aims of the
    Project insofar as they participate in it, and who maintain
    package(s) for the Project or do other work which the Project
    Leader's Delegate(s) consider worthwhile.</p>
  </li>

  <li>
    <p>The Project Leader's Delegate(s) may choose not to admit new
    Developers, or expel existing Developers.  <cite>If the Developers
    feel that the Delegates are abusing their authority they can of
    course override the decision by way of General Resolution - see
    &sect;4.1(3), &sect;4.2.</cite></p>
  </li>
</ol>

<h3>3.3. Procedure</h3>

<p>Developers may make these decisions as they see fit.</p>

<h2><toc-add-entry name="item-4">4. The Developers by way of General Resolution or election</toc-add-entry></h2>

<h3>4.1. Powers</h3>

<p>Together, the Developers may:</p>

<ol>
  <li>
    <p>Appoint or recall the Project Leader.</p>
  </li>

  <li>
    <p>Amend this constitution, provided they agree with a 3:1
    majority.</p>
  </li>

  <li>
    <p>Make or override any decision authorised by the powers of the Project
    Leader or a Delegate.</p>
  </li>

  <li>
    <p>Make or override any decision authorised by the powers of the Technical
    Committee, provided they agree with a 2:1 majority.</p>
  </li>

  <li>
    <p>Issue, supersede and withdraw nontechnical policy documents and
       statements.</p>

    <p>These include documents describing the goals of the project, its
       relationship with other free software entities, and nontechnical
       policies such as the free software licence terms that Debian
       software must meet.</p>

    <p>They may also include position statements about issues of the
    day.</p>
    
    <ol style="list-style: decimal;">
      <li>A Foundation Document is a document or statement regarded as
       critical to the Project's mission and purposes.</li>
      <li>The Foundation Documents are the works entitled <q>Debian
       Social Contract</q> and <q>Debian Free Software Guidelines</q>.</li>
      <li>A Foundation Document requires a 3:1 majority for its
       supersession.  New Foundation Documents are issued and
       existing ones withdrawn by amending the list of Foundation
       Documents in this constitution.</li>
    </ol>

  </li>

  <li>
    <p>Make decisions about property held in trust for purposes
    related to Debian. (See &sect;9.).</p>
  </li>

  <li>
    <p>In case of a disagreement between the project leader and
    the incumbent secretary, appoint a new secretary.</p>
  </li>
</ol>

<h3>4.2. Procedure</h3>

<ol>
  <li>
    <p>The Developers follow the Standard Resolution Procedure, below.
    A resolution or ballot option is introduced if proposed by any
    Developer and sponsored by at least K other Developers, or if
    proposed by the Project Leader or the Technical Committee.</p>
  </li>

  <li>
    <p>Delaying a decision by the Project Leader or their Delegate:</p>

    <ol>
      <li>If the Project Leader or their Delegate, or the Technical
      Committee, has made a decision, then Developers can override them
      by passing a resolution to do so; see &sect;4.1(3).</li>

      <li>If such a resolution is sponsored by at least 2K Developers,
      or if it is proposed by the Technical Committee, the resolution
      puts the decision immediately on hold (provided that resolution
      itself says so).</li>

      <li>If the original decision was to change a discussion period or
      a voting period, or the resolution is to override the Technical
      Committee, then only K Developers need to sponsor the resolution
      to be able to put the decision immediately on hold.</li>

      <li>If the decision is put on hold, an immediate vote is held to
      determine whether the decision will stand until the full vote on
      the decision is made or whether the implementation of the
      original decision will be delayed until then.  There is no
      quorum for this immediate procedural vote.</li>

      <li>If the Project Leader (or the Delegate) withdraws the
      original decision, the vote becomes moot, and is no longer
      conducted.</li>
    </ol>
  </li>

  <li>
    <p>
       Votes are taken by the Project Secretary. Votes, tallies, and
       results are not revealed during the voting period; after the vote
       the Project Secretary lists all the votes cast in sufficient detail
       that anyone may verify the outcome of the election from the votes
       cast. The identity of a Developer casting a particular vote is not
       made public, but Developers will be given an option to confirm that
       their vote is included in the votes cast. The voting period is 2
       weeks, but may be varied by up to 1 week by the Project Leader.
    </p>
  </li>

  <li>
    <p>The Project Leader has a casting vote.  There is a quorum of 3Q.
    The default option is "None of the above."</p>
  </li>

  <li>
    <p>Proposals, sponsors, ballot options, calls for votes and other
    formal actions are made by announcement on a publicly-readable
    electronic mailing list designated by the Project Leader's
    Delegate(s); any Developer may post there.</p>
  </li>

  <li>
    <p>Votes are cast by email in a manner suitable to the Secretary.
    The Secretary determines for each poll whether voters can change
    their votes.</p>
  </li>

  <li>
    <p>Q is half of the square root of the number of current
    Developers.  K is Q or 5, whichever is the smaller.  Q and K need not
    be integers and are not rounded.</p>
  </li>
</ol>

<h2><toc-add-entry name="item-5">5. Project Leader</toc-add-entry></h2>

<h3>5.1. Powers</h3>

<p>The <a href="leader">Project Leader</a> may:</p>

<ol>
  <li>
    <p>Appoint Delegates or delegate decisions to the Technical
    Committee.</p>

    <p>The Leader may define an area of ongoing responsibility or a
    specific decision and hand it over to another Developer or to the
    Technical Committee.</p>

    <p>Once a particular decision has been delegated and made the
    Project Leader may not withdraw that delegation; however, they may
    withdraw an ongoing delegation of particular area of
    responsibility.</p>
  </li>

  <li>
    <p>Lend authority to other Developers.</p>

    <p>The Project Leader may make statements of support for points of
    view or for other members of the project, when asked or otherwise;
    these statements have force if and only if the Leader would be
    empowered to make the decision in question.</p>
  </li>

  <li>
    <p>Make any decision which requires urgent action.</p>

    <p>This does not apply to decisions which have only become
    gradually urgent through lack of relevant action, unless there is a
    fixed deadline.</p>
  </li>

  <li>
    <p>Make any decision for whom noone else has responsibility.</p>
  </li>

  <li>
    <p>Propose General Resolutions and ballot options for General
    Resolutions.  When proposed by the Project Leader, sponsors for the
    General Resolution or ballot option are not required; see
    &sect;4.2.1.</p>
  </li>

  <li>
    <p>Together with the Technical Committee, appoint new members to
    the Committee.  (See &sect;6.2.)</p>
  </li>

  <li>
    <p>Use a casting vote when Developers vote.</p>

    <p>The Project Leader also has a normal vote in such ballots.</p>
  </li>

  <li>
    <p>Vary the discussion period for Developers' votes (as above).</p>
  </li>

  <li>
    <p>Lead discussions amongst Developers.</p>

    <p>The Project Leader should attempt to participate in discussions
    amongst the Developers in a helpful way which seeks to bring the
    discussion to bear on the key issues at hand.  The Project Leader
    should not use the Leadership position to promote their own
    personal views.</p>
  </li>

  <li>
    <p>In consultation with the developers, make decisions affecting
    property held in trust for purposes related to Debian. (See
    &sect;9.). Such decisions are communicated to the members by the
    Project Leader or their Delegate(s). Major expenditures
    should be proposed and debated on the mailing list before
    funds are disbursed.</p>
  </li>
  <li>
    <p>Add or remove organizations from the list of trusted
    organizations (see &sect;9.3) that are authorized to accept and
    hold assets for Debian. The evaluation and discussion leading
    up to such a decision occurs on an electronic mailing list
    designated by the Project Leader or their Delegate(s), on
    which any developer may post. There is a minimum discussion
    period of two weeks before an organization may be added to
    the list of trusted organizations.</p>
  </li>
</ol>

<h3>5.2. Appointment</h3>

<ol>
  <li>The Project Leader is elected by the Developers.</li>

  <li>The election begins six weeks before the leadership post becomes
  vacant, or (if it is too late already) immediately.</li>

  <li>For the first week any Developer may nominate
  themselves as a candidate Project Leader, and summarize their plans for their term.</li>

  <li>For three weeks after that no more candidates may be nominated;
  candidates should use this time for campaigning and discussion.  If
  there are no candidates at the end of the nomination period then the
  nomination period is extended for an additional week, repeatedly if
  necessary.</li>

  <li>The next two weeks are the polling period during which
  Developers may cast their votes.</li>

  <li>The options on the ballot will be those candidates who have
  nominated themselves and have not yet withdrawn, plus None Of The
  Above.  If None Of The Above wins the election then the election
  procedure is repeated, many times if necessary.</li>

  <li>
       The decision will be made using the method specified in &sect;A.5
       of the Standard Resolution Procedure.  The quorum is the same as
       for a General Resolution (&sect;4.2) and the default option is
       <q>None Of The Above</q>.
  </li>

  <li>The Project Leader serves for one year from their election.</li>
</ol>

<h3>5.3. Procedure</h3>

<p>The Project Leader should attempt to make decisions which are
consistent with the consensus of the opinions of the Developers.</p>

<p>Where practical the Project Leader should informally solicit the
views of the Developers.</p>

<p>The Project Leader should avoid overemphasizing their own point of
view when making decisions in their capacity as Leader.</p>

<h2><toc-add-entry name="item-6">6. Technical committee</toc-add-entry></h2>

<h3>6.1. Powers</h3>

<p>The <a href="tech-ctte">Technical Committee</a> may:</p>

<ol>
  <li>
    <p>Decide on any matter of technical policy.</p>

    <p>This includes the contents of the technical policy manuals,
    developers' reference materials, example packages and the behaviour
    of non-experimental package building tools.  (In each case the usual
    maintainer of the relevant software or documentation makes
    decisions initially, however; see 6.3(5).)</p>
  </li>

  <li>
    <p>Decide any technical matter where Developers' jurisdictions
    overlap.</p>

    <p>In cases where Developers need to implement compatible
    technical policies or stances (for example, if they disagree about
    the priorities of conflicting packages, or about ownership of a
    command name, or about which package is responsible for a bug that
    both maintainers agree is a bug, or about who should be the
    maintainer for a package) the technical committee may decide the
    matter.</p>
  </li>

  <li>
    <p>Make a decision when asked to do so.</p>

    <p>Any person or body may delegate a decision of their own to the
    Technical Committee, or seek advice from it.</p>
  </li>

  <li>
    <p>Overrule a Developer (requires a 3:1 majority).</p>

    <p>The Technical Committee may ask a Developer to take a
    particular technical course of action even if the Developer does
    not wish to; this requires a 3:1 majority.  For example, the
    Committee may determine that a complaint made by the submitter of a
    bug is justified and that the submitter's proposed solution should
    be implemented.</p>
  </li>

  <li>
    <p>Offer advice.</p>

    <p>The Technical Committee may make formal announcements about its
    views on any matter.  <cite>Individual members may of course make
    informal statements about their views and about the likely views of
    the committee.</cite></p>
  </li>

  <li>
    <p>Together with the Project Leader, appoint new members to itself
    or remove existing members.  (See &sect;6.2.)</p>
  </li>

  <li>
    <p>Appoint the Chair of the Technical Committee.</p>

    <p>
       The Chair is elected by the Committee from its members. All
       members of the committee are automatically nominated; the
       committee votes starting one week before the post will become
       vacant (or immediately, if it is already too late). The members
       may vote by public acclamation for any fellow committee member,
       including themselves; there is no default option. The vote
       finishes when all the members have voted, or when the voting
       period has ended. The result is determined using the method
       specified in &sect;A.5 of the Standard Resolution Procedure.
       There is no casting vote. If there are multiple options with no
       defeats in the Schwartz set at the end of &sect;A.5.8, the winner
       will be randomly chosen from those options, via a mechanism chosen
       by the Project Secretary.
   </p>
  </li>

  <li>
    <p>The Chair can stand in for the Leader, together with the
    Secretary</p>

    <p>As detailed in &sect;7.1(2), the Chair of the Technical
    Committee and the Project Secretary may together stand in for the
    Leader if there is no Leader.</p>
  </li>
</ol>

<h3>6.2. Composition</h3>

<ol>
  <li>
    <p>The Technical Committee consists of up to 8 Developers, and
    should usually have at least 4 members.</p>
  </li>

  <li>
    <p>When there are fewer than 8 members the Technical Committee may
    recommend new member(s) to the Project Leader, who may choose
    (individually) to appoint them or not.</p>
  </li>

  <li>
    <p>When there are 5 members or fewer the Technical Committee may
    appoint new member(s) until the number of members reaches 6.</p>
  </li>

  <li>
    <p>When there have been 5 members or fewer for at least one week
    the Project Leader may appoint new member(s) until the number of
    members reaches 6, at intervals of at least one week per
    appointment.</p>
  </li>

  <li>
    <p>A Developer is not eligible to be (re)appointed to the Technical
    Committee if they have been a member within the previous 12
    months.</p>
  </li>

  <li>
    <p>If the Technical Committee and the Project Leader agree they
    may remove or replace an existing member of the Technical
    Committee.</p>
  </li>

  <li>
    <p>Term limit:</p>
    <ol>
      <li>
	<p>On January 1st of each year the term of any Committee member
	who has served more than 42 months (3.5 years) and who is one
	of the two most senior members is set to expire on December
	31st of that year.</p>
      </li>
      <li>
	<p>A member of the Technical Committee is said to be more
	senior than another if they were appointed earlier, or were
	appointed at the same time and have been a member of the
	Debian Project longer. In the event that a member has been
	appointed more than once, only the most recent appointment is
	relevant.</p>
      </li>
    </ol>
  </li>
</ol>

<h3>6.3. Procedure</h3>

<ol>
  <li>
    <p>Resolution process.</p>

    <p>The Technical Committee uses the following process to prepare a
    resolution for vote:</p>

    <ol>
      <li>Any member of the Technical Committee may propose a resolution.
      This creates an initial two-option ballot, the other option being
      the default option of "None of the above". The proposer of the
      resolution becomes the proposer of the ballot option.</li>

      <li>Any member of the Technical Committee may propose additional
      ballot options or modify or withdraw a ballot option they
      proposed.</li>

      <li>If all ballot options except the default option are withdrawn,
      the process is canceled.</li>

      <li>Any member of the Technical Committee may call for a vote on the
      ballot as it currently stands. This vote begins immediately, but if
      any other member of the Technical Committee objects to calling for a
      vote before the vote completes, the vote is canceled and has no
      effect.</li>

      <li>Two weeks after the original proposal the ballot is closed to
      any changes and voting starts automatically. This vote cannot be
      canceled.</li>

      <li>If a vote is canceled under &sect;6.3.1.4 later than 13 days
      after the initial proposed resolution, the vote specified in
      &sect;6.3.1.5 instead starts 24 hours after the time of
      cancellation. During that 24 hour period, no one may call for a
      vote, but Technical Committee members may make ballot changes under
      &sect;6.3.1.2.</li>
    </ol>
  </li>

  <li>
    <p>Details regarding voting</p>

    <p>Votes are decided by the vote counting mechanism described in
    &sect;A.5. The voting period lasts for one week or until the
    outcome is no longer in doubt assuming no members change their votes,
    whichever is shorter. Members may change their votes until the voting
    period ends. There is a quorum of two. The Chair has a casting
    vote. The default option is "None of the above".</p>

    <p>When the Technical Committee votes whether to override a Developer
    who also happens to be a member of the Committee, that member may not
    vote (unless they are the Chair, in which case they may use only their
    casting vote).</p>
  </li>

  <li>
    <p>Public discussion and decision-making.</p>

    <p>Discussion, draft resolutions and ballot options, and votes by
    members of the committee, are made public on the Technical
    Committee public discussion list.  There is no separate secretary
    for the Committee.</p>
  </li>

  <li>
    <p>Confidentiality of appointments.</p>

    <p>The Technical Committee may hold confidential discussions via
    private email or a private mailing list or other means to discuss
    appointments to the Committee.  However, votes on appointments must
    be public.</p>
  </li>

  <li>
    <p>No detailed design work.</p>

    <p>The Technical Committee does not engage in design of new
    proposals and policies.  Such design work should be carried out by
    individuals privately or together and discussed in ordinary
    technical policy and design forums.</p>

    <p>The Technical Committee restricts itself to choosing from or
    adopting compromises between solutions and decisions which have
    been proposed and reasonably thoroughly discussed elsewhere.</p>

    <p><cite>Individual members of the technical committee may of
    course participate on their own behalf in any aspect of design and
    policy work.</cite></p>
  </li>

  <li>
    <p>Technical Committee makes decisions only as last resort.</p>

    <p>The Technical Committee does not make a technical decision
    until efforts to resolve it via consensus have been tried and
    failed, unless it has been asked to make a decision by the person
    or body who would normally be responsible for it.</p>
  </li>
  <li>
    <p>Proposing a General Resolution.</p>

    <p>When the Technical Committee proposes a general resolution or a
    ballot option in a general resolution to the project under
    &sect;4.2.1, it may delegate (via resolution or other means agreed on
    by the Technical Committee) the authority to withdraw, amend, or make
    minor changes to the ballot option to one of its members. If it does
    not do so, these decisions must be made by resolution of the Technical
    Committee.</p>
  </li>
</ol>

<h2><toc-add-entry name="item-7">7. The Project Secretary</toc-add-entry></h2>

<h3>7.1. Powers</h3>

<p>The <a href="secretary">Secretary</a>:</p>

<ol>
  <li>
    <p>Takes votes amongst the Developers, and determines the number
    and identity of Developers, whenever this is required by the
    constitution.</p>
  </li>

  <li>
    <p>Can stand in for the Leader, together with the Chair of the
    Technical Committee.</p>

    <p>If there is no Project Leader then the Chair of the
    Technical Committee and the Project Secretary may by joint
    agreement make decisions if they consider it imperative to do
    so.</p>
  </li>

  <li>
    <p>Adjudicates any disputes about interpretation of the
    constitution.</p>
  </li>

  <li>
    <p>May delegate part or all of their authority to someone else, or
    withdraw such a delegation at any time.</p>
  </li>
</ol>

<h3>7.2. Appointment</h3>

<p>The Project Secretary is appointed by the Project Leader and the
current Project Secretary.</p>

<p>If the Project Leader and the current Project Secretary cannot agree
on a new appointment, they must ask the Developers by way of
General Resolution to appoint a Secretary.</p>

<p>If there is no Project Secretary or the current Secretary is
unavailable and has not delegated authority for a decision then the
decision may be made or delegated by the Chair of the Technical
Committee, as Acting Secretary.</p>

<p>The Project Secretary's term of office is 1 year, at which point
they or another Secretary must be (re)appointed.</p>

<h3>7.3. Procedure</h3>

<p>The Project Secretary should make decisions which are fair and
reasonable, and preferably consistent with the consensus of the
Developers.</p>

<p>When acting together to stand in for an absent Project Leader the
Chair of the Technical Committee and the Project Secretary should
make decisions only when absolutely necessary and only when consistent
with the consensus of the Developers.</p>

<h2><toc-add-entry name="item-8">8. The Project Leader's Delegates</toc-add-entry></h2>

<h3>8.1. Powers</h3>

<p>The Project Leader's Delegates:</p>

<ol>
  <li>have powers delegated to them by the Project Leader;</li>

  <li>may make certain decisions which the Leader may not make
  directly, including approving or expelling Developers or designating
  people as Developers who do not maintain packages.  <cite>This is to
  avoid concentration of power, particularly over membership as a
  Developer, in the hands of the Project Leader.</cite></li>
</ol>

<h3>8.2. Appointment</h3>

<p>The Delegates are appointed by the Project Leader and may be
replaced by the Leader at the Leader's discretion.  The Project Leader
may not make the position as a Delegate conditional on particular
decisions by the Delegate, nor may they override a decision made by a
Delegate once made.</p>

<h3>8.3. Procedure</h3>

<p>Delegates may make decisions as they see fit, but should attempt to
implement good technical decisions and/or follow consensus opinion.</p>

<h2><toc-add-entry name="item-9">9. Assets held in trust for Debian</toc-add-entry></h2>

<p>In most jurisdictions around the world, the Debian project is not
in a position to directly hold funds or other property. Therefore,
property has to be owned by any of a number of organisations as
detailed in &sect;9.2.</p>

<p>Traditionally, SPI was the sole organisation authorized to hold
property and monies for the Debian Project. SPI was created in
the U.S. to hold money in trust there.</p>

<p><a href="https://www.spi-inc.org/">SPI</a> and Debian are separate
organisations who share some goals.
Debian is grateful for the legal support framework offered by SPI.</p>

<h3>9.1. Relationship with Associated Organizations</h3>

<ol>
  <li>
    <p>Debian Developers do not become agents or employees of
    organisations holding assets in trust for Debian, or of
    each other, or of persons in authority in the Debian Project,
    solely by the virtue of being Debian Developers. A person
    acting as a Developer does so as an individual, on their own
    behalf. Such organisations may, of their own accord,
    establish relationships with individuals who are also Debian
    developers.</p>
  </li>
</ol>

<h3>9.2. Authority</h3>

<ol>
  <li>
    <p>An organisation holding assets for Debian has no authority
    regarding Debian's technical or nontechnical decisions, except
    that no decision by Debian with respect to any property held
    by the organisation shall require it to act outside its legal
    authority.</p>
  </li>
  <li>
    <p>Debian claims no authority over an organisation that holds
    assets for Debian other than that over the use of property
    held in trust for Debian.</p>
  </li>
</ol>

<h3>9.3. Trusted organisations</h3>

<p>Any donations for the Debian Project must be made to any one of a
set of organisations designated by the Project leader (or a
delegate) to be authorized to handle assets to be used for the
Debian Project.</p>
    
<p>Organisations holding assets in trust for Debian should
undertake reasonable obligations for the handling of such
assets.</p>

<p>Debian maintains a public List of Trusted Organisations that
accept donations and hold assets in trust for Debian
(including both tangible property and intellectual property)
that includes the commitments those organisations have made as
to how those assets will be handled.</p>

<h2><toc-add-entry name="item-A">A. Standard Resolution Procedure</toc-add-entry></h2>

<p>These rules apply to communal decision-making by committees and
plebiscites, where stated above.</p>

<h3>A.0. Proposal</h3>

<ol>
  <li>The formal procedure begins when a draft resolution is proposed and
  sponsored, as specified in &sect;4.2.1.</li>

  <li>This draft resolution becomes a ballot option in an initial
  two-option ballot, the other option being the default option, and the
  proposer of the draft resolution becomes the proposer of that ballot
  option.</li>
</ol>

<h3>A.1. Discussion and Amendment</h3>

<ol>
  <li>The discussion period starts when a draft resolution is proposed and
  sponsored. The minimum discussion period is 2 weeks. The maximum
  discussion period is 3 weeks.</li>

  <li>A new ballot option may be proposed and sponsored according to the
  requirements for a new resolution.</li>

  <li>The proposer of a ballot option may amend that option provided that
  none of the sponsors of that ballot option at the time the amendment is
  proposed disagree with that change within 24 hours. If any of them do
  disagree, the ballot option is left unchanged.</li>

  <li>The addition of a ballot option or the change via an amendment of a
  ballot option changes the end of the discussion period to be one week
  from when that action was done, unless that would make the total
  discussion period shorter than the minimum discussion period or longer
  than the maximum discussion period. In the latter case, the length of
  the discussion period is instead set to the maximum discussion
  period.</li>

  <li>The proposer of a ballot option may make minor changes to that
  option (for example, typographical fixes, corrections of
  inconsistencies, or other changes which do not alter the meaning),
  providing no Developer objects within 24 hours. In this case the length
  of the discussion period is not changed. If a Developer does object, the
  change must instead be made via amendment under &sect;A.1.3.</li>

  <li>The Project Leader may, at any point in the process, increase or
  decrease the minimum and maximum discussion period by up to 1 week from
  their original values in &sect;A.1.1, except that they may not do so in
  a way that causes the discussion period to end within 48 hours of when
  this change is made. The length of the discussion period is then
  recalculated as if the new minimum and maximum lengths had been in place
  during all previous ballot changes under &sect;A.1.1 and &sect;A.1.4.</li>

  <li>The default option has no proposer or sponsors, and cannot be
  amended or withdrawn.</li>
</ol>

<h3>A.2. Withdrawing ballot options</h3>

<ol>
  <li>The proposer of a ballot option may withdraw. If they do, new
  proposers may come forward to keep the ballot option alive, in which
  case the first person to do so becomes the new proposer and any others
  become sponsors if they aren't sponsors already. Any new proposer or
  sponsors must meet the requirements for proposing or sponsoring a new
  resolution.</li>

  <li>A sponsor of a ballot option may withdraw.</li>

  <li>If the withdrawal of the proposer and/or sponsors means that a
  ballot option has no proposer or not enough sponsors to meet the
  requirements for a new resolution, and 24 hours pass without this being
  remedied by another proposer and/or sponsors stepping forward, it is
  removed from the draft ballot. This does not change the length of the
  discussion period.</li>

  <li>If all ballot options except the default option are withdrawn, the
  resolution is canceled and will not be voted on.</li>
</ol>

<h3>A.3. Calling for a vote</h3>
<ol>
  <li>After the discussion period has ended, the Project Secretary will
  publish the ballot and call for a vote. The Project Secretary may do
  this immediately following the end of the discussion period and must do
  so within seven days of the end of the discussion period.</li>

  <li>The Project Secretary determines the order of ballot options and
  their summaries used for the ballot. The Project Secretary may ask
  ballot option proposers to draft those summaries, and may revise them
  for clarity at their discretion.</li>

  <li>Minor changes to ballot options under &sect;A.1.5 may only be made
  if at least 24 hours remain in the discussion period, or if the Project
  Secretary agrees the change does not alter the meaning of the ballot
  option and (if it would do so) warrants delaying the vote. The Project
  Secretary will allow 24 hours for objections after any such change
  before issuing the call for a vote.</li>

  <li>No new ballot options may be proposed, no ballot options may be
  amended, and no proposers or sponsors may withdraw if less than 24 hours
  remain in the discussion period, unless this action successfully extends
  the discussion period under &sect;A.1.4 by at least 24 additional
  hours.</li>

  <li>Actions to preserve the existing ballot may be taken within the last
  24 hours of the discussion period, namely a sponsor objecting to an
  amendment under &sect;A.1.3, a Developer objecting to a minor change
  under &sect;A.1.5, stepping forward as the proposer for an existing
  ballot option whose original proposer has withdrawn it under
  &sect;A.2.1, or sponsoring an existing ballot option that has fewer than
  the required number of sponsors because of the withdrawal of a sponsor
  under point &sect;A.2.2.</li>

  <li>The Project Secretary may make an exception to &sect;A.3.4 and
  accept changes to the ballot after they are no longer allowed, provided
  that this is done at least 24 hours prior to the issuance of a call for
  a vote. All other requirements for making a change to the ballot must
  still be met. This is expected to be rare and should only be done if the
  Project Secretary believes it would be harmful to the best interests of
  the project for the change to not be made.</li>
</ol>

<h3>A.4. Voting procedure</h3>

<ol>
  <li>Options which do not have an explicit supermajority requirement have
  a 1:1 majority requirement.  The default option does not have any
  supermajority requirements.</li>

  <li>The votes are counted according to the rules in &sect;A.5.</li>

  <li>In cases of doubt the Project Secretary shall decide on matters
  of procedure.</li>
</ol>

<h3>A.5. Vote Counting</h3>

<ol>
   <li> Each voter's ballot ranks the options being voted on.  Not all
        options need be ranked.  Ranked options are considered
        preferred to all unranked options.  Voters may rank options
        equally.  Unranked options are considered to be ranked equally
        with one another.  Details of how ballots may be filled out
        will be included in the Call For Votes.
   </li>
   <li> If the ballot has a quorum requirement R any options other
        than the default option which do not receive at least R votes
        ranking that option above the default option are dropped from
        consideration.
   </li>
   <li> Any (non-default) option which does not defeat the default option
        by its required majority ratio is dropped from consideration.
        <ol>
             <li> 
                  Given two options A and B, V(A,B) is the number of voters
                  who prefer option A over option B.
             </li> 
             <li> 
                  An option A defeats the default option D by a majority
                  ratio N, if V(A,D) is greater or equal to  N * V(D,A) and V(A,D) is strictly greater than V(D,A).
            </li> 
             <li> 
                  If a supermajority of S:1 is required for A, its majority ratio
                  is S; otherwise, its majority ratio is 1.
             </li> 
        </ol>
   </li>
   <li> From the list of undropped options, we generate a list of
        pairwise defeats.
        <ol>
             <li>
                  An option A defeats an option B, if V(A,B) is strictly greater
                  than V(B,A).
             </li> 
        </ol>
   </li>
   <li> From the list of [undropped] pairwise defeats, we generate a
        set of transitive defeats.
        <ol>
             <li> 
                  An option A transitively defeats an option C if A defeats
                  C or if there is some other option B where A defeats B AND
                  B transitively defeats C.
             </li> 
        </ol>
   </li>
   <li> We construct the Schwartz set from the set of transitive defeats.
        <ol>
             <li> 
                  An option A is in the Schwartz set if for all options B,
                  either A transitively defeats B, or B does not transitively
                 defeat A.
             </li> 
        </ol>
   </li>
   <li> If there are defeats between options in the Schwartz set,
        we drop the weakest such defeats from the list of pairwise
        defeats, and return to step 5.
        <ol>
             <li> 
                  A defeat (A,X) is weaker than a defeat (B,Y) if V(A,X)
                  is less than V(B,Y).  Also, (A,X) is weaker than (B,Y) if
                  V(A,X) is equal to V(B,Y) and V(X,A) is greater than V(Y,B).
             </li> 
             <li> 
                  A weakest defeat is a defeat that has no other defeat weaker
                  than it.  There may be more than one such defeat.
             </li> 
        </ol>
   </li>
   <li> If there are no defeats within the Schwartz set, then the winner
        is chosen from the options in the Schwartz set.  If there is
        only one such option, it is the winner. If there are multiple
        options, the elector with the casting vote chooses which of those
        options wins.  
   </li>
</ol>

<p>
 <strong>Note:</strong> Options which the voters rank above the default option
 are options they find acceptable.  Options ranked below the default
 options are options they find unacceptable.
</p>

<p><cite>When the vote counting mechanism of the Standard Resolution
Procedure is to be used, the text which refers to it must specify who has
a casting vote, the quorum, the default option, and any supermajority
requirement. The default option must not have any supermajority
requirements.</cite></p>

<h2><toc-add-entry name="item-B">B. Use of language and typography</toc-add-entry></h2>

<p>The present indicative (<q>is</q>, for example) means that the statement
is a rule in this constitution.  <q>May</q> or <q>can</q> indicates that the
person or body has discretion.  <q>Should</q> means that it would be
considered a good thing if the sentence were obeyed, but it is not
binding.  <cite>Text marked as a citation, such as this, is rationale
and does not form part of the constitution.  It may be used only to aid
interpretation in cases of doubt.</cite></p>
