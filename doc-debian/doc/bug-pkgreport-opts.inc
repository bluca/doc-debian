<define-tag bts_severities>
<code>critical</code>, <code>grave</code>, <code>serious</code>, 
<code>important</code>, <code>normal</code>, <code>minor</code>, 
<code>wishlist</code></define-tag>
<define-tag bts_tags>
<code>patch</code>, <code>wontfix</code>, <code>moreinfo</code>, <code>unreproducible</code>, 
<code>help</code>, <code>security</code>, <code>upstream</code>, <code>pending</code>, <code>confirmed</code>, 
<code>ipv6</code>, <code>lfs</code>, <code>d-i</code>, <code>l10n</code>, <code>newcomer</code>, <code>a11y</code>, <code>ftbfs</code>, 
<code>fixed-upstream</code>, <code>fixed</code>, <code>fixed-in-experimental</code>, 
<bts_release_tags>,
<bts_release_ignore_tags></define-tag>
